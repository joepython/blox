# Initial configuration values for DEVELOPMENT mode.

SECRET_KEY    = 'justdevelopmentmode'
RUN_MODE      = 'DEV'
PROD_DATA_DIR = 'blox_data'
PROD_DB_FILE  = 'blox.sqlite'

# Frozen-Flask configuration values

FREEZER_DEV_ROOT = '/Users/tipton/Joe_Dev/blox_dev/dev_blox/staging'
FREEZER_PROD_ROOT = '/Users/tipton/Applications/BLOX/targets'

FREEZER_DESTINATION_ROOT = '/Users/tipton/Applications/BLOX/targets'
FREEZER_REMOVE_EXTRA_FILES = False

SCOPE = 'public'
