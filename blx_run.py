from blx import create_app

blx = create_app()


if __name__ == '__main__':

    print(f'This is blx_run.py ...')

    if blx.config['RUN_MODE'] == 'PROD':
        run_port = 9143
        print(f'\n\n****** Running in PRODUCTION mode on port {run_port}. ******')
        print(f'    DB ---> {blx.config["DATABASE"]}\n\n')
    else:
        run_port = 9101
        print(f'\n\n------ Running in DEVELOPMENT mode on port {run_port}. ------')
        print(f'    dev_DB ---> {blx.config["DATABASE"]}\n\n')

    blx.run(port=run_port, debug=True)
