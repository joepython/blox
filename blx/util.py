# Utility elements and functions

import os

stencil_options = ['blog-std',
                   'Home',
                   'About',
                   'ToC',
                   'Thread ToC',
                   'X-Index',
                   'Concordance',
                   'Tags',
                   'Scope',
                   'all',
                   ]

thread_options = ['BLOX',
                  'utility',
                  '20yrs',
                  'services',
                  'mm_dev',
                  'all',
                  'none',
                  ]

target_options = ['public',
                  'review',
                  'internal',
                  'local']

stencil_functions = {'blog-std': 'blg.standard_blog_post',
                     'Home': 'blg.blog_index',
                     'About': 'blg.blog_about',
                     'ToC': 'blg.blog_toc',
                     'Scope': 'blg.blog_index',
                     }

thread_toc_functions = {'BLOX': 'blg.blog_toc',
                        'utility': 'blg.blog_toc',
                        '20yrs': 'blg.blog_toc',
                        }

scope_options = ['public',
                 'review',
                 'internal',
                 'local',
                 ]


def split_date(post_date):
    post_year, post_month, post_day = post_date.split('-')
    path_date = {'post_year': post_year,
                 'post_month': post_month,
                 'post_day': post_day,
                 }
    return path_date


def dev_or_prod_mode(app):
    """Determine whether running in DEVELOPMENT or PRODUCTION mode.

       Assume DEV mode, which means blox/instance/b2.sqlite database
       Check for existence of PRODUCTION database file.
    """

    app.config['DEV_DB_DIR'] = app.instance_path
    app.config['DEV_DB_FILE'] = 'b2.sqlite'
    app.config['DATABASE'] = os.path.join(app.config['DEV_DB_DIR'],
                                          app.config['DEV_DB_FILE'])

    # PRODUCTION setup under ~/Applications/BLOX.app/... on the Mac
    #
    # i.e.,
    #       ~/Applications/BLOX.app/blox      for project, and
    #       ~/Applications/BLOX.app/blox_data for database

    app.config['PROJECT_DIR'] = os.path.dirname(app.root_path)
    prod_db_file = os.path.join(os.path.dirname(app.config['PROJECT_DIR']),
                                app.config['PROD_DATA_DIR'],
                                app.config['PROD_DB_FILE'])
    if os.path.isfile(prod_db_file):
        app.config['RUN_MODE'] = 'PROD'
        app.config['DATABASE'] = prod_db_file

    # If in PRODUCTION mode, empty file /static/css/dev.css, which
    # changes NavBar color to distinguish DEV mode from PROD mode.

    if app.config['RUN_MODE'] == 'PROD':
        dev_css = os.path.join(app.root_path, 'static/css/dev.css')
        with open(dev_css, 'w') as make_empty:
            make_empty.write('')
        print(f' *_*_*_*_*_*_*_*_ NOTICE: WROTE EMPTY dev.css file _*_*_*_*_*_*_*_*')
