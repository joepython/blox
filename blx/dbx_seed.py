# dbx_if: Interface to blox database

# import datetime
# import logging
import sqlite3

from . import db_map

"""Normalized database operations

    Operations that involve a single spiel entry:
    
      Parameters:
          db == Sqlite database connection for all functions
          spiel == Huddle.d_spiel or Huddle.e_spiel per function
          **kwargs == associated values or condition indicators
    
        Fetch a single spiel -
            . by sid == d_spiel.sid w/ args = {}
            . by date-fname == {post_date: <date>, filename: <fname>}
            . by edited state == {edit_date: True}
        
        Update a single spiel -
            . args == {}
        
        Delete a single spiel -
            . args == {}
        
        Write new spiel - 
            .args == {}
    
        Return: db_error == None --> success
                db_error == <message>
    
    Operations that involve multiple spiel entries:
    
      See docs for db_get_selected_spiels() below.

"""


def db_spiel_update(db, spiel, action=None):
    """

    :param db:
    :param spiel: Huddle.e_spiel
    :param action: 'write', 'update' or 'delete'
    :return: None --> success
             <error message> --> failure
    """
    sql_commands = []
    db_error = None
    if action == 'write':
        return db_write_base_spiel(db, spiel)
    elif action == 'update':
        sql = f'update spiel set filename="{spiel.filename}", '
        sql += f'stencil="{spiel.stencil}", '
        sql += f'title="{spiel.title}", author="{spiel.author}", '
        sql += f'post_date="{spiel.post_date}", tab="{spiel.tab}", '
        sql += f'target="{spiel.target}", thread="{spiel.thread}", '
        sql += f'edit_date="{spiel.edit_date}" where sid={spiel.sid}'
        sql_commands.append(sql)
    elif action == 'delete':
        sql_commands.append(f'delete from stanza where spiel_sid={spiel.sid}')
        sql_commands.append(f'delete from spiel where sid={spiel.sid}')
    else:
        return f'Action "{action}" not recognized.'
    c = db.cursor()
    db.execute('BEGIN')
    try:
        for sql in sql_commands:
            c.execute(sql)
    except sqlite3.Error as db_error:
        print(db_error)
        db.rollback()
    if db_error is None:
        db.commit()
    # db.close()       Let Flask handle closing database.
    return db_error


def db_write_base_spiel(db, spiel):
    """Create new spiel record in database

    Returns: spiel with sid > 0 and db_error == None on success
             db_error message on Sqlite error or duplicate entry
    """

    db_error = db_get_single_spiel(db, spiel,
                                   post_date=True,
                                   filename=True
                                   )
    spiel.sid = 0
    if db_error is None:
        db_error = f'Duplicate date__filename {spiel.post_date}__{spiel.filename}'
        print(db_error)
        return db_error

    # write new spiel entry
    try:
        c = db.cursor()
        c.execute(f'insert into spiel (stencil, post_date, filename, title, author, ' 
                  f'tab, edit_date, publish_date, target, thread) '
                  f'values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                  (spiel.stencil, spiel.post_date, spiel.filename, spiel.title,
                   spiel.author, spiel.tab, spiel.edit_date, spiel.publish_date,
                   spiel.target, spiel.thread)
                  )
    except sqlite3.IntegrityError as err_text:
        db_error = f'Sqlite error: {err_text}'
        spiel.sid = 0
        return db_error
    db.commit()

    db_error = db_get_single_spiel(db, spiel,
                                   post_date=True,
                                   filename=True
                                   )
    # db.close()       Let Flask handle closing database.
    if spiel.sid == 0:
        db_error = f'Database update failed. No new spiel added.'
    return db_error


def sql_execute(db, sql_stmt, sql_tuple=()):
    c = db.cursor()
    db_result = True
    try:
        c.execute(sql_stmt, sql_tuple)
    except sqlite3.Error as db_error:
        print(db_error)
        db_result = db_error
    return db_result


def db_get_single_spiel(db, spiel, **kwargs):

    if not kwargs:
        sql = f'select * from spiel where sid={spiel.sid}'
    elif 'thread' in kwargs:
        sql = f'select * from spiel where thread="{spiel.thread}" '
        sql += f'and stencil="ToC"'
    elif 'stencil' in kwargs:
        sql = f'select * from spiel where stencil="{spiel.stencil}" '
        sql += f'and thread="none"'
    elif 'post_date' in kwargs and 'filename' in kwargs:
        sql  = f'select * from spiel where post_date = "{spiel.post_date}" '
        sql += f'and filename = "{spiel.filename}"'
    else:
        return f'Selection criteria not recognized: {kwargs}'
    c = db.cursor()
    db_error = None
    try:
        c.execute(sql)
        entry = c.fetchall()
        if entry:
            db_spiel = entry[0]
            map_and_fill(c, db_spiel, spiel=spiel)
        else:
            db_error = f'Read single spiel failed: {kwargs}'
    except Exception as db_error:
        print(f'Exception: {db_error}')
    return db_error


def db_search_selected_spiels(db,
                              phrase=None,
                              thread=None,
                              target=None,
                              stencil=None,
                              ):
    """Retrieve spiel list from database - no stanzas

    :param db: BLOX database
    :param phrase: single search phrase string
    :param thread: limit selection to spiels with matching thread values
    :param target: limit selection to spiels with matching target values
    :param stencil: limit selection to spiels with matching stencil values

    Make three sets of spiel IDs -
         - spiels with thread, target and stencil values and with
         - spiels with the search phrase in the title
         - spiels with the search phrase anywhere in Rhyme or Stanza text
       --- the intersection of the first set with the union of the second and
           third sets determines the set of spiels selected

    :return: List of selected spiels - possibly empty
             db_error - None ==> success OR <error message>
    """

    c = db.cursor()

    # Make SELECT statement testing spiel thread, target, stencil filters
    where_and = ''
    thread_str = ''
    target_str = ''
    stencil_str = ''
    sql = 'select sid from spiel '
    if thread is not None:
        where_and = 'where'
        thread_str += f'{where_and} thread="{thread}" '
    if target is not None:
        if where_and:
            where_and = 'and'
        else:
            where_and = 'where'
        target_str = f'{where_and} ({target}) '
    if stencil is not None:
        if where_and:
            where_and = 'and'
        else:
            where_and = 'where'
        stencil_str = f'{where_and} stencil="{stencil}" '
    sql += thread_str + target_str + stencil_str

    try:
        c.execute(sql)
    except sqlite3.Error as sql_error:
        return [], sql_error
    spiel_sid_set = result_group(c.fetchall(), 'sid')
    if not spiel_sid_set:
        return [], "No spiel with Thread, Target, and Stencil values specified."

    # Test for search phrase in spiel title and in stanzas
    if phrase:
        sql = f'select sid from spiel where title like "%{phrase}%"'
        try:
            c.execute(sql)
        except sqlite3.Error as sql_error:
            print(sql_error)
            return [], sql_error
        spiel_phrase_set = result_group(c.fetchall(), 'sid')

        sql = f'select distinct spiel_sid from stanza where part_2 like  ' + \
              f'"%{phrase}%" or (part_1 like "%{phrase}%" and brand="stanza")'
        try:
            c.execute(sql)
        except sqlite3.Error as sql_error:
            print(sql_error)
            return [], sql_error
        stanza_phrase_set = result_group(c.fetchall(), 'spiel_sid')
        phrase_set = spiel_phrase_set.union(stanza_phrase_set)
        sid_set = spiel_sid_set.intersection(phrase_set)
    else:
        sid_set = spiel_sid_set

    if not sid_set:
        return [], "No entries found meeting these criteria."

    # Assemble group element for interpolating into SELECT statement
    sid_group = ''
    for sid in sid_set:
        sid_group += f'{sid}, '
    sid_group = f'({sid_group})'.replace(', )', ')')

    try:
        sql = f'select * from spiel where sid in {sid_group} ' \
              f'order by post_date desc'
        c.execute(sql)
        db_spiels = c.fetchall()
    except sqlite3.Error as sql_error:
        print(sql_error)
        return [], sql_error

    # Assemble list of qualifying spiels based on sid_set members
    spiel_list = []
    for db_spiel in db_spiels:
        spiel = db_map.db_map_spiel(db_spiel)
        spiel_list.append(spiel)
    # db.close()       Let Flask handle closing database.
    return spiel_list, None


def result_group(select_result, rf):
    r = set()
    for result in select_result:
        r.add(result[rf])
    return r


def db_get_selected_spiels(db, **kwargs):
    """Retrieve spiel list from database - no stanzas

    :param db: BLOX database connection
    :param kwargs: Select from the following options:
        args == {}
            (no params -- get all spiels)
        args == {start_date: <date>, end_date: <date>}
            (post_date between specified dates)
        args == {edit_date: <date>}
            (published_date earlier than edit_date)
    :return: List of selected spiels - possibly empty
             db_error - None ==> success OR <error message>
    """
    spiel_list = []
    db_error = None
    c = db.cursor()
    if not kwargs:
        sql = f'select * from spiel order by post_date desc'
    elif 'start_date' in kwargs and 'end_date' in kwargs:
        start_date = kwargs['start_date']
        end_date = kwargs['end_date']
        sql = f'select * from spiel where post_date > "{start_date}" '
        sql += f'and post_date < "{end_date}" order by post_date desc'
    elif 'edit_date' in kwargs:
        sql = f'select * from spiel where publish_date < edit_date '
        sql += f'order by post_date desc'
    try:
        c.execute(sql)
        db_spiels = c.fetchall()
        for db_spiel in db_spiels:
            spiel = db_map.db_map_spiel(db_spiel)
            spiel_list.append(spiel)
    except sqlite3.Error as db_error:
        print(db_error)
    # db.close()       Let Flask handle closing database.
    return spiel_list, db_error


def db_update_stanzas(db, db_actions):
    """Update stanza table as indicated in db_actions

    NOTE: Because part of the key value (seq) can change, updates require
          delete and add
    parm: db_actions = {'deletes': [], 'updates': [], 'adds': []} with a list
          of stanza (Rhyme-Stanza) tuples for each dictionary key value.

    Returns: None     --> Update succeeded
             Message  --> Sqlite error message
    """
    db_error = None
    c = db.cursor()
    db.execute('BEGIN')
    try:
        for tup in db_actions['deletes']:
            sql  = f'delete from stanza where spiel_sid={tup.spiel_sid} and '
            sql += f'brand="{tup.brand}" and seq={tup.d_seq}'
            c.execute(sql)
        for tup in db_actions['updates']:
            sql  = f'delete from stanza where spiel_sid={tup.spiel_sid} and '
            sql += f'brand="{tup.brand}" and seq={tup.d_seq}'
            c.execute(sql)
        for tup in db_actions['updates']:
            sql  = f'insert into stanza (spiel_sid, brand, part_1, part_2, seq, edit_date) '
            sql += f'values (?, ?, ?, ?, ?, ?)'
            c.execute(sql, (tup.spiel_sid, tup.brand, tup.part_1, tup.part_2,
                            tup.re_seq, tup.edit_date))
        for tup in db_actions['adds']:
            sql  = f'insert into stanza (spiel_sid, brand, part_1, part_2, seq, edit_date) '
            sql += f'values (?, ?, ?, ?, ?, ?)'
            c.execute(sql, (tup.spiel_sid, tup.brand, tup.part_1, tup.part_2,
                            tup.re_seq, tup.edit_date))
    except sqlite3.Error as db_error:
        print(db_error)
        db.rollback()
        return db_error
    if db_error is None:
        db.commit()
    # db.close()       Let Flask handle closing database.
    # TODO: log_stanza_changes(db_actions)
    return db_error


def log_stanza_changes(db_actions):
    # print('This is when logging of stanza updates occurs ...')
    return len(db_actions)


def map_and_fill(c, db_spiel, spiel=None):
    """Populate properties of spiel.stanzas

    NOTE: the 'spiel' parm is from huddle.e_spiel
    """
    spiel = db_map.db_map_spiel(db_spiel, spiel)
    spiel.rhymes = []
    spiel.stanzas = []
    sql = f'select * from stanza where spiel_sid={spiel.sid} and brand="rhyme" order by seq'
    c.execute(sql)
    db_rhymes = c.fetchall()
    for db_rhyme in db_rhymes:
        new_rhyme = db_map.db_map_rhyme(db_rhyme)
        spiel.rhymes.append(new_rhyme)
    sql = f'select * from stanza where spiel_sid={spiel.sid} and brand="stanza" order by seq'
    c.execute(sql)
    db_stanzas = c.fetchall()
    for db_stanza in db_stanzas:
        new_stanza = db_map.db_map_stanza(db_stanza)
        spiel.stanzas.append(new_stanza)
    return spiel
