
import os

from flask import (Flask,
                   render_template,
                   request,
                   )

from .util import (dev_or_prod_mode,
                   scope_options,
                   )


def create_app(test_config=None):
    blx = Flask(__name__, instance_relative_config=True)
    blx.config.from_object('config')
    blx.config.from_pyfile('config.py')
    dev_or_prod_mode(blx)

    if test_config is None:
        blx.config.from_pyfile('config.py', silent=True)
    else:
        blx.config.update(test_config)

    try:
        os.makedirs(blx.instance_path)
    except OSError:
        pass

    @blx.route('/blox_local/', methods=['GET', 'POST'])
    def site_index():
        if 'scope' in request.form:
            blx.config['SCOPE'] = request.form['scope']
        else:
            blx.config['SCOPE'] = 'local'
        return render_template('blox_local.html',
                               scope=blx.config['SCOPE'],
                               scope_options=scope_options)

    # register db commands
    from . import db
    db.init_app(blx)

    # register blueprints
    from .generate import gen_bp
    blx.register_blueprint(gen_bp)
    from .spiel_views import spl_bp
    blx.register_blueprint(spl_bp)
    from .stanza_views import stz_bp
    blx.register_blueprint(stz_bp)
    from .routes import blg_bp
    blx.register_blueprint(blg_bp)

    return blx
