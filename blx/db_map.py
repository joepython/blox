""" db_map.py - move DB record items to objects

    Database records items are sqlite3.Row types, addressable
    by column names.

"""


def db_map_spiel(db_spiel, spiel=None):
    from .huddle import Spiel
    if spiel is None:
        spiel = Spiel()
    spiel.sid          = db_spiel['sid']
    spiel.stencil      = db_spiel['stencil']
    spiel.filename     = db_spiel['filename']
    spiel.title        = db_spiel['title']
    spiel.author       = db_spiel['author']
    spiel.post_date    = db_spiel['post_date']
    spiel.tab          = db_spiel['tab']
    spiel.edit_date    = db_spiel['edit_date']
    spiel.publish_date = db_spiel['publish_date']
    spiel.thread       = db_spiel['thread']
    spiel.target       = db_spiel['target']
    return spiel


def db_map_rhyme(db_rhyme):
    from .huddle import Rhyme
    rhyme = Rhyme()
    rhyme.spiel_sid  = db_rhyme['spiel_sid']
    rhyme.brand      = db_rhyme['brand']
    rhyme.part_1     = db_rhyme['part_1']
    rhyme.part_2     = db_rhyme['part_2']
    rhyme.d_seq      = db_rhyme['seq']
    rhyme.e_seq      = db_rhyme['seq']
    rhyme.edit_date  = db_rhyme['edit_date']
    return rhyme


def db_map_stanza(db_stanza):
    from .huddle import Stanza
    stanza = Stanza()
    stanza.spiel_sid  = db_stanza['spiel_sid']
    stanza.brand      = db_stanza['brand']
    stanza.part_1     = db_stanza['part_1']
    stanza.part_2     = db_stanza['part_2']
    stanza.d_seq      = db_stanza['seq']
    stanza.e_seq      = db_stanza['seq']
    stanza.edit_date  = db_stanza['edit_date']
    return stanza
