
import json
# import logging
import os

from flask import (Blueprint,
                   flash,
                   current_app,
                   redirect,
                   render_template,
                   request,
                   session,
                   url_for,
                   )

from .huddle import Huddle

gen_bp = Blueprint('gen', __name__)

huddle = Huddle()


@gen_bp.route('/generate/')
def generate_setup():
    return render_template('generate/generate_options.html', spiel_list=[])


@gen_bp.route('/generate_all/', methods=['GET', 'POST'])
def generate_all():
    external_generate_all()
    flash(f'Generation complete ...')
    return redirect(url_for('gen.generate_setup', spiel_list=[]))


# Generation process switched to "generate_all()"-only
'''
@gen_bp.route('/gen_new/', methods=['GET', 'POST'])
def generate_edited_spiels():
    # Create list of edited spiels
    # Send list to gen_selected page
    spiel_list = huddle.get_edited_spiels()
    session['generate'] = []
    for spiel in spiel_list:
        session['generate'].append(spiel.sid)
    return render_template('generate/gen_confirm.html', spiel_list=spiel_list)


@gen_bp.route('/gen_dates/', methods=['GET', 'POST'])
def generate_date_spiels():
    # Create list of spiels between dates
    # Send list to gen_selected page
    start_date = request.form['start_date']
    end_date = request.form['end_date']
    spiel_list = huddle.get_spiels_date_range(start_date, end_date)
    session['generate'] = []
    for spiel in spiel_list:
        session['generate'].append(spiel.sid)
    return render_template('generate/gen_confirm.html', spiel_list=spiel_list)


@gen_bp.route('/gen_selected/', methods=['GET', 'POST'])
def generate_selected_spiels():
    # Show list of specified spiels
    # Option to run all or select/reverse subset
    if 'reverse' in request.form:
        session['reverse'] = True
    else:
        session['reverse'] = False
    spiel_list = assemble_spiels_for_generation(request.form,
                                                session['generate'])
    external_generation(spiel_list)
    return f"Sent list {spiel_list} to generator<br/><a href='/generate/'>Back to Generate</a>"
'''


def external_generation(spiel_list):
    spiel_nos = []
    for sid in spiel_list:
        spiel_nos.append(sid)
    if spiel_nos:
        fd = open('spiel_nos.txt', 'w')
        fd.write(json.dumps(spiel_nos))
        fd.close()
        freeze_file = os.path.join(current_app.config['PROJECT_DIR'], 'freeze.py')
        print(f'freeze_file = {freeze_file}')
        os.system(f'python {freeze_file}')
    return 'Generated!'


def external_generate_all():
    freeze_file = os.path.join(current_app.config['PROJECT_DIR'], 'freeze.py')
    print(f'freeze_file = {freeze_file}')
    os.system(f'python {freeze_file}')
    return


def assemble_spiels_for_generation(candidates, sids):
    """Select rhymes for editing based on values in request.form

    NOTE: Edit ALL depends on REVERSE + No selections
    """
    selected_sids = []
    for n in sids:
        if str(f'spiel_{str(n)}') in candidates:
            if session['reverse']:
                continue
            selected_sids.append(n)
        else:
            if session['reverse']:
                selected_sids.append(n)
    return selected_sids
