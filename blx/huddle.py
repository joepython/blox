
from collections import namedtuple
from copy import deepcopy
import datetime
# import logging

from flask import g

from .db import get_db
from . import dbx_seed


# NOTE: Have to keep these tuples synched with the db schema
# TODO: Build tuples using db introspection

tup_spiel  = namedtuple('SPIEL',  ['sid', 'stencil', 'filename', 'title', 'author',
                                   'post_date', 'tab', 'edit_date',
                                   'publish_date', 'target', 'thread'])

tup_stanza = namedtuple('STANZA', ['spiel_sid', 'brand', 'part_1', 'part_2',
                                   'd_seq', 're_seq', 'edit_date'])


class Huddle:

    def __init__(self):
        self.d_spiel   = Spiel()
        self.e_spiel   = deepcopy(self.d_spiel)
        self.edit_date = None

    def __repr__(self):
        rep = '\n\nHUDDLE EDIT SETUP -----------------------------------\n'
        rep += f'Current DB {self.d_spiel.__repr__(False)}'
        rep += f'Edited {self.e_spiel.__repr__(False)}'
        rep += self.e_spiel.__repr__()
        rep += '\n\nEND-----------------------------------HUDDLE EDIT SETUP \n'

        return rep

    @property
    def spiel(self):
        for stanza in self.e_spiel.stanzas:
            stanza.add_display_supplements()
        for rhyme in self.e_spiel.rhymes:
            rhyme.add_display_supplements()
        return self.e_spiel

    '''
        Get multiple spiels using different criteria.
        
        Return spiel_list[] and db_error 
        
        db_error == None --> success 
        
        db_error == <message> and spiel_list --> failure
        
    '''

    @staticmethod
    def get_all_spiels():
        get_db()
        return dbx_seed.db_get_selected_spiels(g.db)

    @staticmethod
    def get_spiels_date_range(start_date, end_date):
        get_db()
        return dbx_seed.db_get_selected_spiels(g.db,
                                               start_date=start_date,
                                               end_date=end_date
                                               )

    @staticmethod
    def get_selected_spiels(phrase=None, thread=None,
                            target=None, stencil=None):
        if phrase == '':
            phrase = None
        if thread == 'all':
            thread = None
        if stencil == 'all':
            stencil = None
        if target == 'local':
            target = None
        elif target == 'internal':
            target = 'target="internal" or target="review" or target="public"'
        elif target == 'review':
            target = 'target="review" or target="public"'
        elif target == 'public':
            target = 'target="public"'

        get_db()
        return dbx_seed.db_search_selected_spiels(g.db,
                                                  phrase,
                                                  thread,
                                                  target,
                                                  stencil,
                                                  )

    @staticmethod
    def get_edited_spiels():
        get_db()
        return dbx_seed.db_get_selected_spiels(g.db,
                                               edit_date=True,
                                               )
    '''
        Get single spiel using different criteria.
        
        db_error == None --> success
        
        On failure, Huddle.d_spiel.sid set to 0 (zero) --> failure
        
    '''

    def get_spiel_by_sid(self, sid):
        get_db()
        self.new_spiel()
        self.d_spiel.sid = sid
        db_error = dbx_seed.db_get_single_spiel(g.db,
                                                self.d_spiel,
                                                )
        db_error = self.spiel_load(db_error)
        return self.spiel, db_error

    def get_spiel_by_post_date_filename(self, post_date, filename):
        get_db()
        self.d_spiel.post_date = post_date
        self.d_spiel.filename = filename
        db_error = dbx_seed.db_get_single_spiel(g.db,
                                                self.d_spiel,
                                                post_date=True,
                                                filename=True
                                                )
        db_error = self.spiel_load(db_error)
        return self.spiel, db_error

    def get_toc_spiel(self, thread):
        get_db()
        self.d_spiel.thread = thread
        db_error = dbx_seed.db_get_single_spiel(g.db,
                                                self.d_spiel,
                                                thread=True
                                                )
        db_error = self.spiel_load(db_error)
        return self.spiel, db_error

    def get_unthreaded_spiel(self, stencil):
        get_db()
        self.d_spiel.stencil = stencil
        db_error = dbx_seed.db_get_single_spiel(g.db,
                                                self.d_spiel,
                                                stencil=True
                                                )
        db_error = self.spiel_load(db_error)
        return self.spiel, db_error

    def spiel_load(self, db_error):
        if db_error is not None:
            self.d_spiel.sid = 0
        self.e_spiel = deepcopy(self.d_spiel)
        return db_error

    def update_spiel(self, spiel):
        """Update spiel table entry with current huddle.e_spiel values

        """
        get_db()
        spiel.edit_date = datetime.datetime.utcnow()
        db_error = dbx_seed.db_spiel_update(g.db,
                                            spiel,
                                            action='update'
                                            )

    def publish_spiel(self, spiel):
        """Update spiel table entry with new publish_date
           from generation process.

        """
        get_db()
        db_error = dbx_seed.db_spiel_update(g.db,
                                            spiel,
                                            action='update'
                                            )
        if db_error is None:
            self.d_spiel = deepcopy(self.e_spiel)
        return db_error

    def new_spiel(self):
        self.d_spiel = Spiel()
        self.e_spiel = deepcopy(self.d_spiel)
        self.edit_date = None
        return None

    def make_new_spiel(self, spiel):
        """Create new DB entry and initialize Huddle.

        """
        get_db()
        self.e_spiel = spiel
        self.e_spiel.edit_date    = datetime.datetime.utcnow()
        self.e_spiel.publish_date = "2018-09-07 00:01:01.010000"
        db_error = dbx_seed.db_spiel_update(g.db,
                                            self.e_spiel,
                                            action='write')
        if self.e_spiel.sid > 0 and db_error is None:
            self.d_spiel = deepcopy(self.e_spiel)
        return db_error

    def delete_spiel(self, sid):
        if sid != self.e_spiel.sid:
            return self.spiel, True
        get_db()
        db_error = dbx_seed.db_spiel_update(g.db,
                                            self.e_spiel,
                                            action='delete'
                                            )
        return self.spiel, db_error
    
    def update_stanzas(self, spiel, brand):
        """Update database stanzas with huddle.e_spiel.stanzas

        Returns: False --> No changes to update
                 None --> All database actions succeeded
                 <message> --> Some database action failed
                              [see db_stanzas.db_update_stanzas()]


        """

        if not self.e_spiel == spiel:
            db_error = f'Expected spiel == huddle.e_spiel:\n<br/>'
            db_error += f'    huddle.e_spiel={self.e_spiel}\n<br/>'
            db_error += f'             spiel={spiel}'
            return spiel, db_error
        self.e_spiel = spiel
        db_actions = self.check_stanza_changes(brand)
        if db_actions is False:
            return spiel, False

        get_db()
        db_error = dbx_seed.db_update_stanzas(g.db,
                                              db_actions)
        if db_error is None:
            db_error = dbx_seed.db_spiel_update(g.db,
                                                self.e_spiel,
                                                action='update'
                                                )
            if db_error is None:
                self.d_spiel = deepcopy(self.e_spiel)
                self.edit_date = None
        return self.spiel, db_error

    def check_stanza_changes(self, brand):
        """Compare edited version of stanzas against original stanzas

           Determine whether dealing with spiel.stanzas[] or spiel.rhymes[]
           based on 'brand' parameter.

           Return: False --> Nothing updated
                   <message
        """
        db_actions = {'deletes': [], 'updates': [], 'adds': []}
        stanza_change_time = datetime.datetime.utcnow().isoformat(' ')
        self.edit_date = None
        if brand == 'stanza':
            db_stanzas = self.d_spiel.stanzas
            ed_stanzas = self.e_spiel.stanzas
        elif brand == 'rhyme':
            db_stanzas = self.d_spiel.rhymes
            ed_stanzas = self.e_spiel.rhymes
        else:
            return False
        ed_stanzas = self.re_sequence_stanzas(ed_stanzas)
        for e_stanza in ed_stanzas:
            r = tup_stanza(self.e_spiel.sid, brand, e_stanza.part_1,
                           e_stanza.part_2, e_stanza.d_seq, e_stanza.re_seq,
                           stanza_change_time)
            if e_stanza.e_seq == 0:           # deleted stanza
                db_actions['deletes'].append(r)
                continue
            if e_stanza.d_seq == 0:           # new stanza
                db_actions['adds'].append(r)
                continue
            for d_stanza in db_stanzas:
                if d_stanza.d_seq != e_stanza.d_seq:   # not the d_stanza we
                    continue                           # are looking for...
                if e_stanza.re_seq != d_stanza.d_seq or \
                   e_stanza.part_1 != d_stanza.part_1 or  \
                   e_stanza.part_2 != d_stanza.part_2:
                        db_actions['updates'].append(r)
        if db_actions['deletes'] or \
           db_actions['adds']or \
           db_actions['updates']:
                self.edit_date = stanza_change_time
        else:
            db_actions = False
        return db_actions

    @staticmethod
    def re_sequence_stanzas(r_list):
        """Resequence stanzas[] and rhymes[] for updating

        NOTE: Works for both because of common d_seq and e_seq attributes
        """
        # TODO: Not working for "Add a New Rhyme"
        o_list = []
        for r in r_list:
            o_list.append((r.e_seq, r.d_seq))
        o_dict = dict(sorted(o_list))           # sort on edited sequence value
        n = 1100
        for e_seq in o_dict:
            if e_seq == 0:      # For deletion - do  not resequence
                continue
            for stanza in r_list:
                if stanza.e_seq == e_seq:
                    stanza.re_seq = n
                    n += 100
                    # Don't break here: e_seq may be duplicated in two stanzas
        return r_list


class Stanza:

    html_patterns = {
                     'stanza_header': '''<div class="card my-4-blx">
      <h5 class="card-header" style="background: #444444; color: white"><strong>[[]]</strong></h5>''',
                     'stanza_body': '''
      <div class="card-body">
        [[]]
      </div>
    </div>''',
                      }

    def __init__(self, seq=0):
        self.brand     = 'stanza'
        self.d_seq     = seq
        self.e_seq     = seq
        self.re_seq    = 0
        self.part_1    = ''
        self.part_2    = ''
        self.edit_date = ''
        self.height    = 0

    def make_new_from_kwargs(self, d):
        self.brand     = 'stanza'
        self.d_seq     = d['d_seq']
        self.e_seq     = d['e_seq']
        self.re_seq    = d['re_seq']
        self.part_1    = d['part_1']
        self.part_2    = d['part_2']
        self.edit_date = d['edit_date']
        self.height    = 0

    def __repr__(self):
        rep  = f'    d_seq={self.d_seq} e_seq={self.e_seq} re_seq={self.re_seq} '
        rep += f'header={self.part_1[0:20]} body={self.part_2[0:30]}...\n'
        return rep


    def add_display_supplements(self):

        # This calculation is based on trial and error - just approximate
        divisor = 44
        lines = self.part_2.count('\n') / 2
        self.height = lines + 2 + len(self.part_2) / divisor
        # print(f'Code height = {lines} + 2 + {len(self.part_2)}/{divisor} = {self.height}')

    def present(self):
        result = self.html_patterns['stanza_header'].replace('[[]]', self.part_1)
        result += self.html_patterns['stanza_body'].replace('[[]]', self.part_2)
        return result


class Rhyme(Stanza):

    html_patterns = {'graph': '<div>[[]]<br/><br/></div>',
                     'code': '''
<div style="background-color: #e6e6e6"><div style="; margin-left: 10px">
<pre><code style="font-size: 16px; font-family: Courier, monospace">
[[]]
</code></pre></div></div>''',
                     'blue_box': '''<div style="background-color: #edf2f7"><div style="margin-left: 12px; margin-right: 10px"><br/>
[[]]
</div><br/></div><hr>''',
                     'lead_text': '<div style="font-size: 1.1rem">[[]]<br/><br/></div>',
                     'straight_html': '[[]]',
                     'h1': '''<h1>[[]]</h1>''',
                     'h2': '''<h2>[[]]</h2>''',
                     'h3': '''<h3>[[]]</h3>''',
                     'h4': '''<h4>[[]]</h4>''',
                     'h5': '''<h5>[[]]</h5>''',
                     'h6': '''<h6>[[]]</h6>''',
                     }

    def __init__(self, seq=0):
        super().__init__(seq)
        self.brand    = 'rhyme'
        self.dropdown = ''
        self.height   = 0

    def __repr__(self, full=True):
        rep = f'D_seq={self.d_seq} RE_seq={self.re_seq} E_seq={self.e_seq}  '
        rep += f'mold={self.part_1}  verse={self.part_2[0:30]}...\n'
        return rep

    def make_new_from_kwargs(self, d):
        super().make_new_from_kwargs(d)
        self.brand    = 'rhyme'
        self.dropdown = ''
        self.height   = 0

    def add_display_supplements(self):
        # TODO: Pull dd from database or config

        dd = ['graph', 'code', 'lead_text', 'blue_box', 'straight_html',
              'h1', 'h2', 'h3', 'h4', 'h5', 'h6']
        self.dropdown = ''
        for p in dd:
            if p == self.part_1:
                self.dropdown += f'    <option value="{p}" selected>{p}</option>\n'
            else:
                self.dropdown += f'    <option value="{p}">{p}</option>\n'

        # This calculation is based on trial and error - just approximate
        divisor = 66
        if self.part_1 == 'code':
            divisor = 55
        lines = self.part_2.count('\n') / 2
        self.height = lines + 2 + len(self.part_2) / divisor
        # print(f'Code height = {lines} + 2 + {len(self.part_2)}/{divisor} = {self.height}')
        return

    def present(self):
        result = self.html_patterns[self.part_1].replace('[[]]', self.part_2)
        return result


class Spiel:

    def __init__(self, sid=0):
        self.sid = sid
        self.stencil = ''
        self.post_date = ''
        self.author = 'joe'
        self.filename = ''
        self.tab = ''
        self.title = ''
        self.rhymes = []
        self.stanzas = []
        self.edit_date = ''
        self.publish_date = ''
        self.thread = ''
        self.target = ''

    def __repr__(self, full=True):
        rep = ''
        rep += f'Spiel: {self.title}\n'
        rep += f'         sid = {self.sid}            stencil = {self.stencil}\n'
        rep += f'    filename = {self.filename}\n      author = {self.author}\n'
        rep += f'        date = {self.post_date}\n        tab = {self.tab}\n'
        rep += f'        thread = {self.thread}\n        target = {self.target}\n'
        if full:
            rep += 'Rhymes:\n'
            for rhyme in self.rhymes:
                rep += rhyme.__repr__()
            rep += 'Stanzas:\n'

            for stanza in self.stanzas:
                rep += stanza.__repr__()
        else:
            rep += f'        (full=False)\n'
        return rep

    def __log__(self):
        rep  = f'spiel log: '
        rep += f'{self.sid}, {self.post_date}, {self.author}, '
        rep += f'{self.edit_date}, {self.publish_date}, '
        rep += f'{self.thread}, {self.target}, '
        rep += f'rhymes={len(self.rhymes)}, stanzas={len(self.stanzas)}, '
        rep += f'"{self.filename}", "{self.title}"'
        return rep

    def export(self, export_name):
        o_str = ''
        div = '::'
        o_str += f'spiel{div}{self.post_date}{div}{self.author}{div}{self.thread}\n'
        for stanza in self.stanzas:
            o_str += f'stanza{div}{stanza.d_seq}{div}{stanza.part_1}{div}{stanza.part_2}\n'
        for rhyme in self.rhymes:
            o_str += f'rhyme{div}{rhyme.d_seq}{div}{rhyme.part_1}{div}{rhyme.part_2}\n'
        try:
            with open(export_name, 'w') as e_file:
                e_file.write(o_str)
        except Exception as file_error:
            return file_error
        return True
