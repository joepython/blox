# import logging

from datetime import datetime
import os

from flask import (Blueprint,
                   current_app,
                   flash,
                   redirect,
                   render_template,
                   request,
                   send_from_directory,
                   url_for,
                   )

from .huddle import (Huddle,
                        )
from .util import (scope_options,
                      split_date,
                      )

blg_bp = Blueprint('blg', __name__)

huddle = Huddle()


@blg_bp.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(current_app.root_path, 'static'),
                               'favicon.ico',
                               mimetype='image/vnd.microsoft.icon'
                               )

@blg_bp.route('/blox')
def blox_start():

    return render_template('start.html')

@blg_bp.route('/')
def blog_index():

    spiel, db_error = huddle.get_unthreaded_spiel('Home')

    if db_error is not None:
        flash(f'No post found for "index.html"<br/>    {db_error}.')

    segments, stanzas = display_setup(spiel)

    return render_template('index.html',
                           spiel=spiel,
                           segments=segments,
                           stanzas=stanzas
                           )


@blg_bp.route('/about.html')
def blog_about():

    spiel, db_error = huddle.get_unthreaded_spiel('About')

    if db_error is not None:
        flash(f'No post found for: "about.html"<br/>    {db_error}.')

    segments, stanzas = display_setup(spiel)

    return render_template('about.html',
                           spiel=spiel,
                           segments=segments,
                           stanzas=stanzas
                           )


@blg_bp.route('/scope.html', methods=['GET', 'POST'])
def blog_scope():

    spiel, db_error = huddle.get_unthreaded_spiel('Scope')

    if db_error is not None:
        flash(f'No post found for: "scope.html"<br/>    {db_error}.')

    if 'scope' in request.form:
        current_app.config['SCOPE'] = request.form['scope']

    scope_val = current_app.config['SCOPE']

    segments, stanzas = display_setup(spiel)

    return render_template('scope.html',
                           spiel=spiel,
                           segments=segments,
                           stanzas=stanzas,
                           scope_val=scope_val,
                           scope_options=scope_options
                           )


@blg_bp.route('/blog/<thread_filename>')
def blog_toc(thread_filename):
    """Make ToC for each thread

        NOTE: Distinguish valid entries based on 'scope' as indicated by
        the .config['SCOPE'] value - which defaults to 'public' (but is
        resettable on the /blox_local/ page to change 'live' views).
    """
    thread = thread_filename[0:-5]
    toc_target = current_app.config['SCOPE']
    toc_spiel, db_result = huddle.get_toc_spiel(thread)
    if db_result is not None:
        flash(db_result)
        return redirect(url_for('blg.blog_index'))
    spiel_list, spiel_error = huddle.get_all_spiels()
    if spiel_error is not None:
        flash(spiel_error)
        return redirect(url_for('blg.blog_index'))

    toc_entries = []
    for spiel in spiel_list:
        if spiel.stencil in ['ToC', 'Home', 'About', 'Scope']:
            continue
        if spiel.thread != thread:
            continue
        if toc_target == 'public' and spiel.target != 'public':
            continue
        if toc_target == 'review' and spiel.target in ['internal', 'local']:
            continue
        if toc_target == 'internal' and spiel.target == 'local':
            continue
        std_path = split_date(spiel.post_date)
        std_path['post_fname'] = spiel.filename
        std_path['title'] = spiel.title
        toc_entries.append(std_path)

    stanzas = []
    for stanza in toc_spiel.stanzas:
        stanzas.append(stanza.present())

    p = {'gen_date': str(datetime.utcnow())[0:19],
         'tab': toc_spiel.tab,
         'title': toc_spiel.title,
         }
    return render_template('blog/std_toc.html',
                           thread=thread,
                           paths=toc_entries,
                           p=p,
                           stanzas=stanzas)


@blg_bp.route('/blog/<post_year>/<post_month>/<post_day>/<post_fname>')
def standard_blog_post(post_year, post_month, post_day, post_fname):
    """Build standard blog post page for blox from database

    This function is used by Frozen Flask to generate a full set
    of pages for standard blog posts.  A standard blog post is
    identified by a year, month, day, and fname.

    """
    post_date = f'{post_year}-{post_month}-{post_day}'
    spiel, db_result = huddle.get_spiel_by_post_date_filename(post_date, post_fname)

    if db_result is not None:
        return f'No post found for: [{post_year}/{post_month}/{post_day}/{post_fname}]'

    segments, stanzas = display_setup(spiel)
    bna, db_result = before_and_after(spiel)
    if db_result is not None:
        return db_result

    return render_template('blog/standard_post.html',
                           spiel=spiel,
                           segments=segments,
                           stanzas=stanzas,
                           bna=bna,
                           )


def display_setup(spiel):

    spiel.publish_date = str(datetime.utcnow())[0:19]

    segments = []
    for rhyme in spiel.rhymes:
        segments.append(rhyme.present())

    stanzas = []
    for stanza in spiel.stanzas:
        stanzas.append(stanza.present())

    return segments, stanzas


def before_and_after(spiel):

    bna = {'all': [],
           'a10': [],
           'thread': [],
           't10': [],
          }
    spiel_list, spiel_error = huddle.get_all_spiels()
    if spiel_error is not None:
        flash(spiel_error)
        return bna, spiel_error
    all_ten_list = spiel_list[0:10]

    thread_list, spiel_error = \
        huddle.get_selected_spiels(None,
                                   spiel.thread,
                                   current_app.config['SCOPE'],
                                   None,
                                  )
    if spiel_error is not None:
        flash(spiel_error)
        return bna, spiel_error
    thread_ten_list = thread_list[0:10]


    bna = {'all': spiel_list,
           'a10': all_ten_list,
           'thread': thread_list,
           't10': thread_ten_list,
          }


    return bna, spiel_error
